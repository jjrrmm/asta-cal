import math
import cairo

from datetime import datetime, timedelta

WIDTH, HEIGHT = 291, 210

surface = cairo.PDFSurface("asta-calender.pdf", WIDTH, HEIGHT)
ctx = cairo.Context(surface)
ctx.scale(WIDTH, HEIGHT)  # Normalizing the canvas

start_date = datetime(2019, 7, 15)
end_date = datetime(2019, 7, 15) + timedelta(days=365)

weekdays = [
    "Montag",
    "Dienstag",
    "Mitwoch",
    "Donnerstag",
    "Freitag",
    "Sonnabend",
    "Sonntag",
]

def write_some_text(begin, end):
    days = [begin + timedelta(days=i) for i in range(7)]
    left_days = [begin + timedelta(days=i) for i in range(4)]
    right_days = [begin + timedelta(days=i) for i in range(4, 7)]

    ctx.set_source_rgb(0, 0, 0)
    ctx.set_font_size(0.02)

    ctx.select_font_face(
        "Helvetica",
        cairo.FONT_SLANT_NORMAL,
        cairo.FONT_WEIGHT_NORMAL,
    )

    padding = 0.1
    item_height = (1 - (padding * 2)) / 4
    for i, day in enumerate(left_days):
        ctx.move_to(padding, padding + i * item_height)
        ctx.show_text(weekdays[i])
        ctx.show_text(day.isoformat())

    ctx.set_source_rgb(1, 0, 0)
    for i, day in enumerate(right_days):
        draw_at = 1 - padding, padding + i * item_height
        ctx.move_to(*draw_at)
        text_to_show = weekdays[i + 4] + day.isoformat()
        show_text_right(draw_at, text_to_show)
        ctx.show_text(text_to_show)

def show_text_right(pos, s):
    xbearing, ybearing, width, height, dx, dy = ctx.text_extents(s)
    current_x, current_y =  pos
    ctx.move_to(current_x - width, current_y)

def draw_stuff_test():
    ctx.move_to(0.5, 0.5)
    ctx.line_to(1, 1)  # Line to (x,y)
    ctx.close_path()
    ctx.set_source_rgb(0.3, 0.2, 0.5)  # Solid color
    ctx.set_line_width(0.02)
    ctx.stroke()

def make_weeks(start, end):
    weeks = []
    begin = start
    while begin < end:
        weeks.append((begin, end))
        begin += timedelta(days=7)
    return weeks

for week in make_weeks(start_date, end_date):
    write_some_text(*week)
    surface.show_page()

